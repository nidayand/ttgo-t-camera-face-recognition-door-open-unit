#include <Arduino.h>
#include <WiFi.h>
WiFiClient client;

// WIFI Manager
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h>
#define WIFI_ACCESSPOINT_NAME   "GarageDoorAP"

// OLED display
#include "SSD1306.h"
#include "OLEDDisplayUi.h"
#define SSD1306_ADDRESS 0x3c
#define I2C_SDA 21
#define I2C_SCL 22
SSD1306 oled(SSD1306_ADDRESS, I2C_SDA, I2C_SCL);
OLEDDisplayUi ui(&oled);

// PIR
// PIR will only show display as it doesn't work welll
//  when the device is outside
#define PIN_PIR     33
#define ENABLE_PIR  true        //Use PIR detection to turn on screen
boolean pirTriggered = false;   //Used to turn off display when inactive
unsigned long pirTriggeredStart = 0;     //Variable to hold start of screen on

// Right Button
#define LONGPRESS_RESET_WIFI_ENABLED  true //Disable if you don't want this feature
#include "OneButton.h"
#define BUTTON_1 34
OneButton button(BUTTON_1, true);

// Camera
#include "esp_camera.h"
#define PWDN_GPIO_NUM   26
#define RESET_GPIO_NUM  -1
#define XCLK_GPIO_NUM   32
#define SIOD_GPIO_NUM   13
#define SIOC_GPIO_NUM   12

#define Y9_GPIO_NUM     39
#define Y8_GPIO_NUM     36
#define Y7_GPIO_NUM     23
#define Y6_GPIO_NUM     18
#define Y5_GPIO_NUM     15
#define Y4_GPIO_NUM     4
#define Y3_GPIO_NUM     14
#define Y2_GPIO_NUM     5
#define VSYNC_GPIO_NUM  27
#define HREF_GPIO_NUM   25
#define PCLK_GPIO_NUM   19

// JSON for parsing the server response
#include "ArduinoJson.h"

// OLED display messages
#define TXT_VALIDATING  "Skickar bild\nför analys..."
#define TXT_WELCOME     "Välkommen"
#define TXT_UNKNOWN     "Okänd!"
#define TXT_STARTUP     "Tryck på knappen\nför att låsa\nupp dörren"
#define TXT_CAMERAFAIL  "Kamera init fel!"
#define TXT_INIT        "Startar..."
#define TXT_CONFIGAP    "Please configure\nvia Access Point:"
#define TXT_RESETWIFI   "Resetting WIFI!"
#define TXT_SERVERERR   "Felaktigt serversvar!"
#define TXT_SERVERNOCONNECT   "Kan inte ansluta till\n Face Recognition server!"
#define TXT_SERVERNORESPONSE   "Inget svar från servern!"
#define TXT_MQTT_CONNECTED      "Ansluten till\nMQTT server!"
#define TXT_MQTT_NOT_CONNECTED "Kan inte ansluta till\nMQTT server!"
boolean screenOnDelayRunning = false;   //Used to turn off display when inactive
unsigned long screenDelayStart = 0;     //Variable to hold start of screen on
#define SCREENONSECS    30              //Will turn off screen if no activity after X secs

// Face recognition server
// https://hub.docker.com/r/nidayand/facerecognition
const char * host = "192.168.2.244";
const uint16_t port = 9093;
const char * uploadpath = "/upload";

// MQTT server
#include <PubSubClient.h>
PubSubClient mqttClient(client);
#define MQTT_SERVER     "192.168.2.244"
#define MQTT_PORT       1883
char mqttClientId[15];          //Create a Unique MQTTClient id from MAC address
String mqtt_topic_send;
String mqtt_topic_callback;


static camera_config_t camera_config = {
    .pin_pwdn  = PWDN_GPIO_NUM,
    .pin_reset = RESET_GPIO_NUM,
    .pin_xclk = XCLK_GPIO_NUM,
    .pin_sscb_sda = SIOD_GPIO_NUM,
    .pin_sscb_scl = SIOC_GPIO_NUM,

    .pin_d7 = Y9_GPIO_NUM,
    .pin_d6 = Y8_GPIO_NUM,
    .pin_d5 = Y7_GPIO_NUM,
    .pin_d4 = Y6_GPIO_NUM,
    .pin_d3 = Y5_GPIO_NUM,
    .pin_d2 = Y4_GPIO_NUM,
    .pin_d1 = Y3_GPIO_NUM,
    .pin_d0 = Y2_GPIO_NUM,
    .pin_vsync = VSYNC_GPIO_NUM,
    .pin_href = HREF_GPIO_NUM,
    .pin_pclk = PCLK_GPIO_NUM,

    //XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .xclk_freq_hz = 20000000,
    .ledc_timer = LEDC_TIMER_0,
    .ledc_channel = LEDC_CHANNEL_0,

    .pixel_format = PIXFORMAT_JPEG,//YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size = FRAMESIZE_SVGA,//QQVGA-QXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 12, //0-63 lower number means higher quality
    .fb_count = 1 //if more than one, i2s runs in continuous mode. Use only with JPEG
};

/**
 * Writes text to OLED display
 * if small is used 5 rows works
 * if large is used 3 rows works
 * */
void writeOLED(String txt, boolean small = false, boolean showwelcome = false){
    oled.displayOn();
    screenDelayStart = millis();
    screenOnDelayRunning = true;
    oled.clear();
    oled.setFont(small ? ArialMT_Plain_10 : ArialMT_Plain_16);
    oled.setTextAlignment(TEXT_ALIGN_CENTER);
    int16_t y = oled.getHeight() / 2;
    //Find how many rows in text
    int startIndex = 0;
    int counter = 0;
    while (txt.indexOf("\n", startIndex) != -1) {
        startIndex = txt.indexOf("\n", startIndex) + String("\n").length();
        counter += 1;
    }
    int subtractY = 0;
    subtractY = (small ? 10 + (counter * 5): 12 + (counter*8));
    oled.drawString(oled.getWidth() / 2, y - subtractY, txt);
    oled.display();
    if (showwelcome){
        delay(5000);
        writeOLED(TXT_STARTUP);
    }
}

/** 
 * Create unique MQTT Client id
 */
void createMQTTClientId() {
    uint64_t chipid=ESP.getEfuseMac();//The chip ID is essentially its MAC address(length: 6 bytes).
    uint16_t chip = (uint16_t)(chipid>>32);
    snprintf(mqttClientId,15,"ESP32-%04X",chip);
}

void mqttCallback(char *topic, byte *payload, int length){
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    String res = "";
    for (int i = 0; i < length; i++){
        Serial.print((char) payload[i]);
        res += String((char) payload[i]);
    }
    //Print larege with reset to welcome
    writeOLED(res, false, true);
}

void wifiConfigModeCallback (WiFiManager *myWiFiManager) {
    oled.clear();
    Serial.println("Entered config mode");
    //if you used auto generated SSID, print it
    Serial.println(myWiFiManager->getConfigPortalSSID());
    writeOLED(String(TXT_CONFIGAP)+"\n"+myWiFiManager->getConfigPortalSSID(), true);
}

boolean mqttConnect(){
    if (!mqttClient.connected()) {
        Serial.println("Attempting MQTT connection...");
        if (mqttClient.connect(mqttClientId)){
            Serial.println("Connected to MQTT server!");
            //Setup subscriptions
            mqttClient.subscribe(mqtt_topic_callback.c_str());
            
            //Send registration msg
            mqttClient.publish("/raw/esp32/register", ("{ \"id\": \"" 
                + String(mqttClientId) + "\", \"ip\":\"" 
                + WiFi.localIP().toString() 
                + "\", \"type\":\"esp32\", \"description\":\"Door interface for picture\"}").c_str());
            return true;
        } else return false;
    }
    return true;
}

boolean mqttPublish(const char* topic, const char* payload){
    if (mqttConnect()){
        //Send msg
        mqttClient.publish(topic, payload);
        return true;
    } else {
        writeOLED(TXT_MQTT_NOT_CONNECTED, true, true);
        return false;
    }
}

/*
*   Reset wifi settings
*/
void longpressStop(){
    Serial.println("Resetting WIFI credentials...");
    WiFiManager wifiManager;
    wifiManager.resetSettings();

    writeOLED(TXT_RESETWIFI, true);
    delay(2000);
    oled.clear();
    ESP.restart();
}

void takePicture(){
    Serial.println("Taking picture...");
    delay(200);
    //acquire a frame
    camera_fb_t * fb = esp_camera_fb_get();
    if (!fb) {
        ESP_LOGE(TAG, "Camera Capture Failed");
        return;
    }

    //Sending message
    writeOLED(TXT_VALIDATING);

    //Submitting request
    char status[64] = {0};
    struct tm timeinfo;
    strftime(status, sizeof(status), "%Y%m%d%H%M%S", &timeinfo);

    String request_content = "--------------------------ef73a32d43e7f04d\r\n";
    request_content+="Content-Disposition: form-data; name=\"file\"; filename=\"file.jpg\"\r\n";
    request_content+="Content-Type: image/jpeg\r\n\r\n";
    const char* rc = request_content.c_str();

    String request_end = "\r\n--------------------------ef73a32d43e7f04d--\r\n";
    const char* re = request_end.c_str();

    uint32_t content_len = fb->len + strlen(rc) + strlen(re);

    String request = "POST "+String(uploadpath)+" HTTP/1.1\r\n";
    request += "Host: "+String(host)+"\r\n";
    request += "User-Agent: curl/7.54.0\r\n";
    request += "Accept: */*\r\n";
    request += "Expect: 100-continue\r\n";
    request += "Content-Length: " + String(content_len);
    request += "\r\nContent-Type: multipart/form-data; boundary=------------------------ef73a32d43e7f04d\r\n";
    request += "\r\n";
    //request += "Connection: close\r\n\r\n";

    if (!client.connect(host, port)) {
        Serial.println("Connection failed");
        client.stop();
        esp_camera_fb_return(fb);
        writeOLED(TXT_SERVERNOCONNECT, true, true);
        return;
    }

    if (!client.connected()){
        Serial.println("Connection failed");
        client.stop();
        esp_camera_fb_return(fb);
        writeOLED(TXT_SERVERNOCONNECT, true, true);
        return; 
    }

    Serial.print(request);
    client.print(request);

    // Do not need to wait for 100 continue
    // https://tools.ietf.org/html/rfc7231#section-5.1.1

    // Send attachment with headers and closing
    Serial.print(rc);
    client.print(rc);

    uint8_t *image = fb->buf;
    size_t size = fb->len;
    size_t offset = 0;
    size_t ret = 0;
    while (1)
    {
        ret = client.write(image + offset, size);
        offset += ret;
        size -= ret;
        if (fb->len == offset)
        {
            break;
        }
    }
    Serial.print(re);
    client.print(re);

    client.find("\r\n");

    // Read server response
    boolean ok = false;
    String msg = "";

    unsigned long timeout = millis();
    while(client.available()) {
        // Check if it takes too long
        if ((millis() - timeout) > 10000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            esp_camera_fb_return(fb);
            writeOLED(TXT_SERVERNORESPONSE, true);
            return;
        }
        String line = client.readStringUntil('\r');

        if (line.indexOf("200 OK")>-1){
            Serial.println("OK");
            ok = true;
        }
        if (line.indexOf("{")> -1 && line.indexOf("}")>-1){
            msg = line;
            break;
        }
    }
    client.stop();
    Serial.println(msg);

    esp_camera_fb_return(fb);

    // Evaluate the response
    if (ok){
        //Parse JSON
        StaticJsonDocument<200> doc;

        // Deserialize the JSON document
        DeserializationError error = deserializeJson(doc, msg);

          // Test if parsing succeeds.
        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }

        // Get first value
        String person = doc["items"][0];

        // If person = null unknown person
        if (doc["items"][0].isNull()){
            writeOLED(TXT_UNKNOWN, false, true);

        } else {
            //Fix first char to uppercase
            person.toUpperCase();
            writeOLED(String(TXT_WELCOME)+"\n"+person, false, true);

            //Publish instruction
            if (!mqttPublish(mqtt_topic_send.c_str(), String("unlock").c_str())){
                writeOLED("Failed to unlock!\nCannot send unlock message", true, true);
            }
        }

    } else {
        writeOLED(TXT_SERVERERR, true, true);
    }
}

void setup() {
    Serial.begin(115200);
    Serial.setDebugOutput(true);

    // Initializing OLED
    // Docs: https://github.com/ThingPulse/esp8266-oled-ssd1306
    oled.init();
    writeOLED(TXT_INIT);

    // Setup button
    // https://github.com/mathertel/OneButton
    if (LONGPRESS_RESET_WIFI_ENABLED){              //Reset WIFI through long press
        button.attachLongPressStop(longpressStop);
    }
    button.attachClick(takePicture);                //Take picture

    // Initializing Camera
    esp_err_t err = esp_camera_init(&camera_config);
    if (err != ESP_OK) {
        Serial.printf("Camera init Fail");
        writeOLED(TXT_CAMERAFAIL);
        while (1);
    }
    sensor_t * s = esp_camera_sensor_get();
    //initial sensors are flipped vertically and colors are a bit saturated
    {
        s->set_vflip(s, 1);//flip it back
        s->set_brightness(s, 1);//up the blightness just a bit
        s->set_saturation(s, -2);//lower the saturation
    }

    // Setup PIR
    if (ENABLE_PIR){
        pinMode(PIN_PIR, INPUT);
    }

    // Setup MQTT
    mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
    mqttClient.setCallback(mqttCallback);
    createMQTTClientId();
    mqtt_topic_send = "/raw/esp32/"+String(mqttClientId)+"/action";
    mqtt_topic_callback = "/raw/esp32/"+String(mqttClientId)+"/callback";

    // WiFiManager
    WiFiManager wifiManager;
    wifiManager.setAPCallback(wifiConfigModeCallback);
    wifiManager.setConnectTimeout(10);
    wifiManager.autoConnect(WIFI_ACCESSPOINT_NAME);
    
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");

}

boolean firstLoop = true;
void loop() {
    //First loop means that WIFI is set properly and
    //we need to set the welcome screen. We do this in the
    //loop as it will crash after wifiManager in setup()
    if (firstLoop){
        // Print welcome
        firstLoop = false;
        writeOLED(TXT_STARTUP);
    }

    //Reconnect MQTT
    if (!mqttConnect()){
        writeOLED(TXT_MQTT_NOT_CONNECTED, true);
        Serial.println("Cannot connect to MQTT server");
        Serial.println(" try again in 5 seconds");
        // Wait 5 seconds before retrying
        delay(5000);
    }
    mqttClient.loop();

    // Turn off display
    if (screenOnDelayRunning && ((millis() - screenDelayStart)>(SCREENONSECS*1000))){
        screenOnDelayRunning = false;
        oled.displayOff();
    }

    // Check for PIR movement
    if (ENABLE_PIR){
        if (pirTriggered && (millis() - pirTriggeredStart) > 10000){
            pirTriggered = false;
        }
        if (!pirTriggered && digitalRead(PIN_PIR)==1){
            Serial.println("Turns on display");
            pirTriggered = true;
            pirTriggeredStart = millis();

            screenDelayStart = millis();
            screenOnDelayRunning = true;
            oled.displayOn();
        }
    }

    // // put your main code here, to run repeatedly:
    // int remainingTimeBudget = ui.update();

    // if (remainingTimeBudget > 0) {
    //     //Drawing

    // }

    //Listen for button actions
    button.tick();
}