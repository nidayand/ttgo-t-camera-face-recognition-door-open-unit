# TTGO T-Camera Face Recognition Door Open Unit

TTGO T-Camera device is used to take a picture, send picture to Face Recognition Service API for validation and if successful response send an MQTT message to open a door lock.

## Using components of TTGO T-Camera
- Camera
- Wifi
- PIR
- Right button
- OLED display

## Dependecies
See lib_deps secion in [platformio.ini](https://gitlab.com/nidayand/ttgo-t-camera-face-recognition-door-open-unit/blob/master/platformio.ini).
It is using the Development branch of WiFiManager in order to get ESP32 support.

## Behaviour
- Connect to WIFI. If not successful start an Access Point for configuration. Use any device to connect to Access Point for WIFI configuration. Default name = GarageDoorAP
- Right button serves two purposes
  - Long press - will reset WIFI configuration (can be disabled)
  - Short press - takes picture
- When picture is taken, image will be posted to your [Face Recognition Service API](https://hub.docker.com/r/nidayand/facerecognition) for evaulation of if the face is known
- If person is known an MQTT message will be sent to the device that controls the door lock servo. Message: ```/raw/esp32/{unique id}/action : unlock```
- Listens to ```/raw/esp32/{unique id}/callback : {msg}``` to feedback msg on screen
- If there is no activity the screen will be turned off. Default 30s
- If there is movement detected by the PIR (can be turned off by configuration) the screen is turned on

## Reguirements
- A [TTGO T-Camera](https://github.com/lewisxhe/esp32-camera-series) device
- A [Face Recognition Service API](https://hub.docker.com/r/nidayand/facerecognition) server
- An MQTT server. The implementation is not using MQTT Auth but that is easy for you to add in the mqttConnect method. See [pubsubclient example](https://github.com/knolleary/pubsubclient/blob/master/examples/mqtt_auth/mqtt_auth.ino)
- (A client that subscribes and takes action on the sent MQTT message)

## Configure
All configuration parameters are set in the beginning of the main.cpp file.

```
#define ENABLE_PIR  true

#define LONGPRESS_RESET_WIFI_ENABLED  true

#define TXT_VALIDATING          "Sending pic\nfor analysis..."
#define TXT_WELCOME             "Welcome"
#define TXT_UNKNOWN             "Unknown!"
#define TXT_STARTUP             "Press button\nto unlock"
#define TXT_CAMERAFAIL          "Camera init error!"
#define TXT_INIT                "Starting..."
#define TXT_CONFIGAP            "Please configure\nvia Access Point:"
#define TXT_RESETWIFI           "Resetting WIFI!"
#define TXT_SERVERERR           "Incorrect server\nresponse!"
#define TXT_SERVERNOCONNECT     "Cannot connect to\n Face Recognition server!"
#define TXT_SERVERNORESPONSE    "No response from server!"
#define TXT_MQTT_CONNECTED      "Connected to\nMQTT server!"
#define TXT_MQTT_NOT_CONNECTED  "Cannot connect to\nMQTT server!"

#define SCREENONSECS    30

//Face Recognition server address
const char * host = "192.168.2.244";
const uint16_t port = 9093;
const char * uploadpath = "/upload";

//MQTT server address
#define MQTT_SERVER     "192.168.2.244"
#define MQTT_PORT       1883


```

